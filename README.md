# Project Healthy  

![](screenshots/Login.png)  

Simple aplication for android 9.0+ for tracking diet

# Functions

-Registration  
-Loging to user account  
-Adding and managing user meals  
-Adding and managing user goals  
-Tracking user goal for each day during a week  
-Creating and managing application notifications  
-History of eaten meals by user  
-Changing user password  
-Deleting user  

![](screenshots/Menu.png)  
![](screenshots/Add_Meal.png)  
![](screenshots/Add_Meal_Added.png)  
![](screenshots/Add_Meal_Dropdown.png)  
![](screenshots/Statistics.png)  
![](screenshots/Options.png)  
![](screenshots/Target.png)  
![](screenshots/Target_Dropdown.png)  
![](screenshots/Notifications.png)  
![](screenshots/Notifications_Manage.png)  
![](screenshots/History.png)  
